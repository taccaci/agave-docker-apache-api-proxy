## Agave Apache AOU Proxy

This is the proxy server used to expose Agave's backend APIs to the outside world. It contains an Apache server with SSL enabled and rewrite rules to each of Agave's  API core science API containers.

## What is the Agave Platform?

The Agave Platform ([http://agaveapi.co](http://agaveapi.co)) is an open source, science-as-a-service API platform for powering your digital lab. Agave allows you to bring together your public, private, and shared high performance computing (HPC), high throughput computing (HTC), Cloud, and Big Data resources under a single, web-friendly REST API.

* Run scientific codes

  *your own or community provided codes*

* ...on HPC, HTC, or cloud resources

  *your own, shared, or commercial systems*

* ...and manage your data

  *reliable, multi-protocol, async data movement*

* ...from the web

  *webhooks, rest, json, cors, OAuth2*

* ...and remember how you did it

  *deep provenance, history, and reproducibility built in*

For more information, visit the [Agave Developer’s Portal](http://agaveapi.co) at [http://agaveapi.co](http://agaveapi.co).


## Using this image

This image will set up an Apache server that proxies connections to the backend Agave APIs. The connections to all APIs happen via standard http rewrite.

### Running this image

If you are developing with this image, simply start the image with the default settings.

```
docker run -h docker.example.com
           -p 80:80 \
           -p 443:443 \
           --name proxy \
           -v $(pwd)/ssl:/ssl:ro \
           -v $(pwd)/logs/proxy:/var/log/apache2 \
           agaveapi/apache-api-proxy:alpine
```

### SSL Support

To add ssl support, volume mount your ssl cert, key, ca cert file, and ca chain file as needed. In the following example, a folder containing the necessary files is volume mounted to /ssl in the container.

```
docker run -h docker.example.com \
           -p 80:80 \
           -p 443:443 \
           --name proxy \
           -v `pwd`/ssl:/ssl:ro \
           -v $(pwd)/logs/proxy:/var/log/apache2 \
           -e SSL_CERT=/ssl/docker_example_com_cert.cer \
           -e SSL_KEY=/ssl/docker.example.com.key \
           -e SSL_CA_CERT=/ssl/docker_example_com.cer \
           agaveapi/apache-api-proxy:alpine
```

### Logging

All apache access and error logs are written to /var/log/apache2. You man access them by mounting the folder as a host volume.
